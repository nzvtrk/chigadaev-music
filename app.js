const express = require('express')
const app = express()
const port = 3000;
const mailer = require('./server/mailer')

app.use(express.json())
app.use(express.static('dist'));

app.post('/api/send', async (req, res) => {
  try {
    console.log('sendind message from', req.body.email)
    await mailer.sendmail(req.body)
    await res.json({result: 'ok'})
  } catch (e) {
    res.status(500)
  }
});

app.listen(port, () => {
  console.log(`Stared at ${port}`)
});
