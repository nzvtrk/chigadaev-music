const nodemailer = require('nodemailer');
require('dotenv').config()


const transporter = nodemailer.createTransport({
  host: process.env.SMTP_SERVER,
  port: 465,
  secure: true,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS,
  },
})

const getTemplate = data => `
  Новая заявка
  <br><br>
  Имя: ${data.name} <br>
  Email: ${data.email} <br>
  Курс: ${data.course} <br>
`
// Сообщение: ${data.message} <br>

const sendmail = (body) =>
  transporter.sendMail({
    from: process.env.SMTP_USER,
    to: process.env.FEEDBACK_MAIL,
    subject: `Заявка music-school.chigadaev.com`,
    html: getTemplate(body),
  })

module.exports = {
  sendmail,
}
