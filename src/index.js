import SmoothScroll from 'smooth-scroll'

function sendForm() {
  const data = {
    name: document.querySelector('.js-name').value,
    email: document.querySelector('.js-email').value,
    message: document.querySelector('.js-message').value,
    // course
  }
  console.log('data', data)

  fetch('/api/send', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(data)
  }).then(r => r.json())
    .then(() => alert('Сообщение отправлено'))
    .catch(() => alert('Произошла ошибка'));
}

const scroll = new SmoothScroll('a[href*="#"]');

window.sendForm = sendForm
window.course = null
